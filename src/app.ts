import TileLayer from "ol/layer/tile";
import Map from "ol/map";
import XYZ from "ol/source/xyz";
import View from "ol/view";

const map = new Map({
    layers: [
        new TileLayer({
            source: new XYZ({
                url: "https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            }),
        }),
    ],
    target: "map",
    view: new View({
        center: [0, 0],
        zoom: 2,
    }),
});
